![Build Status](https://gitlab.com/uvacreate/amsterdam-time-machine/iiif-level0-maps/badges/main/pipeline.svg)

---
# Overview of images served as IIIF level0
Temporary storage and publication of IIIF level0 images for the Amsterdam Time Machine project. Image `info.json` descriptions can be requested at `https://uvacreate.gitlab.io/amsterdam-time-machine/iiif-level0-maps/iiif/{image-name}/info.json`.

## Generate IIIF level0 tiles

`iiif-tiler.jar` version 1.0.1 is a Java application that generates IIIF level0 tiles from a source image. The source image is a local file. See: https://github.com/glenrobson/iiif-tiler/releases/tag/1.0.1 (Licensed under [Apache License 2.0](https://github.com/glenrobson/iiif-tiler/blob/main/LICENSE))

### Command

```bash
java -jar iiif-tiler.jar -tile_size 128 -version 3 -identifier https://uvacreate.gitlab.io/amsterdam-time-machine/iiif-level0-maps/iiif/
```

## List of images

* `010194001652.jpg` - Haarlemmerweg 4-10A, De nieuwe gasfabriek (Westergasfabriek), plattegrond. (1885) Downloaded from  from https://archief.amsterdam/beeldbank/detail/b60bf29b-9c13-d6a3-5514-d5969c760edc